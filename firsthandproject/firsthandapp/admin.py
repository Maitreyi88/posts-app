from django.contrib import admin
from .models import *

# Register your models here.
#$ register your models here to see the data
# make sure you have a super user registered
# run 'python manage.py createsuperuser'

admin.site.register(UserModel)
admin.site.register(UserModelDetails)
admin.site.register(Post)