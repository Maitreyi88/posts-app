from django.apps import AppConfig


class FirsthandappConfig(AppConfig):
    name = 'firsthandapp'
