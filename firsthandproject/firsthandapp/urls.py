from django.urls import path
from .views import *


#$ add your URLs here

urlpatterns = [
    
    path("accounts/login/", login_view, name="login"),
    path("accounts/logout/", logout_view, name="logout"),
    path("accounts/register/",register, name="register"),
    #$ add url paths below here, do not forget to add "name" parameter
    path('',welcome, name="welcome"),
    path('add/',add_details,name="add"),
    path('addpost/',add_post,name='addpost'),
    path('all/',all,name="allposts"),
    path('new/',new,name="newposts"),
    path('your/',your,name="yourposts"),
    path('filter/department/',dep_view,name="filteredd"),
    path('filter/year/',year_view,name="filteredy"),
]