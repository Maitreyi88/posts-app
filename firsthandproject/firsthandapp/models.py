from django.db import models
from django.contrib.auth.models import User



"""
The __str__() methods are commented since there are no fields included.
You have to just uncomment the definitions to get the code working.
"""


# These are the choices for some fields in the Database.
# refer the ./db.png image.
CONST_year_choices={
    ("FE","First Year"),
    ("SE","Second Year"),
    ("TE","Third Year"),
    ("BE","Final Year"),
}

CONST_department_choices={
    ("IT","Information Technology"),
    ("CS","Computer Science"),
    ("EE","Electrical"),
    ("PR","Printing"),
    ("ME","Mechanical"),
    ("EN","Electronics and Telecommunication"),
}

#$ Refer ./db.png for detailed models
#  The basic template is added here, just add the relevant fields

class UserModelDetails(models.Model):
    """UserModelDetails Table
    """

    joined_on=models.DateField(
        verbose_name="Joined on",
        null=False,
    )
    year =models.CharField(
    max_length=2,
    choices=CONST_year_choices,
    blank=True,null=True,
    default=1
    )
    department=models.CharField(
    max_length=2,
    choices=CONST_department_choices,
    blank=True,null=True,
    default=1
    )
    city=models.CharField(
    max_length=50,null=True,
    )
    favourite_color=models.CharField(
     max_length=50,
     blank=True,
    )

    def __str__(self) -> str:
        return f"Person in {self.year} in {self.department}"

class UserModel(models.Model):
    """UserModel Table

    """
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        verbose_name= 'user',
    
        #$ Add other details
    )
    phonenumber=models.DecimalField(max_digits=10, decimal_places=0,null=True)
    details=models.ForeignKey(UserModelDetails,on_delete=models.CASCADE)

    #$ Add other fields


    def __str__(self) -> str:
        return str(self.user)


class Post(models.Model):
    """Post Table
    """
    user=models.ForeignKey(
           to=User,
        on_delete=models.CASCADE,
    )
    title=models.CharField(max_length=100,blank=True)
    details=models.CharField(max_length=1000,blank=True)
    time=models.DateTimeField(auto_now=True)
    deleted=models.BooleanField(null=True)
    userdetails=models.ForeignKey(UserModelDetails,on_delete=models.CASCADE ,default=None)
    def __str__(self) -> str:
        return f"Post made by {self.user}"

'''
Make sure you have added this app into INSTALLED_APPS for migrations to happen

Also register these models in ./admin.py
'''

