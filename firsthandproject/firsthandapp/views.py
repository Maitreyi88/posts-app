from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.forms import modelformset_factory
from .models import *
from datetime import date,time
from django.utils import timezone
import datetime
from django.contrib import messages


# This the login page. The user gets to see this login page if he 
# tries to access the system. Go through the working of the view.
def login_view(request):


    if request.method == "GET":
        # If the request is of type GET, then just show the login page
        return render(request, 'firsthandapp/login.html')


    if request.method == "POST":
        # If the request is of type POST, i.e. when the user has filled 
        # the form and wants to sign in, we check if the user is registered.


        try:
            # Try block, just in case anything goes wrong!??


            username = request.POST["username"] # Extract the username
            password = request.POST["password"] # Extract the password
          

            # Try authenticating the User
            user = authenticate(request, username=username, password=password)


            if user is not None:

                # If user exists, then log him in.
                login(request, user)

                try:
                    # After login is complete, try redirecting to the URL requested by user earlier
                    return redirect(request.GET["next"])

                except:

                    # else redirect to welcome page
                    return redirect("/")

            else:
                # if user is not valid, print "Login Failed" in console.
                #$ Implement a way to convey failed login to the user.
                messages.error(request, 'Invalid Account details',fail_silently=True,)
                return redirect('/accounts/login/')


        except:
            # If the POST request data is invalid, or any other unforseen error, render HTTP
            messages.error(request, 'Invalid Account details', fail_silently=True,)
            return redirect('/accounts/login/')



# The following decorator
# @login_required()
# renders the view only if the user has logged in. Else it redirects user to login page.
# it is useful in keeping site safe. Right?
@login_required(login_url='/accounts/login')
def all(request):
    context={}

    first10posts=Post.objects.all().filter(deleted=False)[::-1]
    
    context['posts'] = first10posts
    # posts = Posts.objects.all()
    # context={ 'posts': posts }
    return render(request,"firsthandapp/allposts.html",context)





@login_required(login_url='/accounts/login') # you need to be logged-in first to be logged-out
def logout_view(request):

    logout(request) # log out the user

    # return HttpResponse("<b>Logged out successfully</b>")
    return redirect('/accounts/login')





#$ To register a User, implement the following view.
# You can try using ModelForms, or build your own custom view.


def register(request):
    if request.method == 'POST':
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']
        username=request.POST['username']
        email=request.POST['email']
        password1=request.POST['password1']
        password2=request.POST['password2']
        if password1==password2:
            if User.objects.filter(username=username).exists():
                print("username taken")
                messages.error(request, 'Username Taken',fail_silently=True,)
            elif User.objects.filter(email=email).exists():
                print("email taken")
                messages.error(request, 'Email Taken',fail_silently=True,)
            else:
                user=User.objects.create_user(username=username,password=password1,email=email,first_name=first_name,last_name=last_name)
                user.save()
                print('user created')
                return redirect("/")
        else:
             messages.error(request, 'Password match failed',fail_silently=True,)
    else:
        pass
       
    return render(request, 'firsthandapp/register.html')
    



#$ Implement a beautiful welcome page.
# show the name of the user in the main area.
@login_required(login_url='/accounts/login')
def welcome(request):
    context={}
    pinfo=UserModel.objects.filter(user=request.user)
    context['pinfo'] = pinfo
    return render(request, "firsthandapp/welcome.html",context)


@login_required(login_url='/accounts/login')
def add_details(request):
    context={}
    year_choices=CONST_year_choices
    dep_choices=CONST_department_choices
    context['year_choices']=year_choices
    context['dep_choices']=dep_choices
    
    if UserModel.objects.filter(user=request.user):
             messages.error(request, 'User Details Exist,cannot modify',fail_silently=True,)
             return redirect('/')
    else:
             if request.method == 'POST':
                department=request.POST.get('department')
                year=request.POST.get('year')
                phonenumber=request.POST['phonenumber']
                joined_on=request.POST['joined_on']
                favourite_color=request.POST['favourite_color']
                city=request.POST['city']

        # if UserModel.objects.filter(user=request.user):
        #      messages.error(request, 'User Details Exist',fail_silently=True,)
             
                
                usermodeldetails=UserModelDetails.objects.create(
                                department=department,
                                year=year,
                                joined_on=joined_on,
                                favourite_color=favourite_color,
                                city=city
                            )
                
                    
                if usermodeldetails: 
                            usermodel=UserModel.objects.create(user=request.user, phonenumber=phonenumber, details=usermodeldetails)
                            return redirect('/')
                print(department,year)
        
             
    
             return render(request, "firsthandapp/add_details.html",context)


@login_required(login_url='/accounts/login')
def add_post(request):
    if request.method == 'POST':
            title=request.POST.get('title')
            details=request.POST.get('description')
            time=datetime.datetime.now()
            pinfo=UserModel.objects.filter(user=request.user)
            for i in pinfo:
              userdetails=i.details
            post=Post.objects.create(user=request.user, details=details,time=time, title=title, deleted=False,userdetails=userdetails)
            messages.success(request, 'Post created successfully',fail_silently=True,)
    return render(request,"firsthandapp/addpost.html",{})


#$ implement a view where only posts added today are shown
@login_required(login_url='/accounts/login')    
def new(request):

    today_filter=Post.objects.filter(time__date=date.today(),deleted=False)[::-1]

    return render(request,"firsthandapp/newposts.html",{"new":today_filter})


#$ implement a view where only users' posts are shown
@login_required(login_url='/accounts/login')
def your(request):
    your_posts=Post.objects.filter(user=request.user,deleted=False)[::-1]
    return render(request,"firsthandapp/your.html",{'your':your_posts})


#$ implement a view where users can filter the posts
# filter the posts by 
# 1. Departments of User
# 2. Year of User
# In case there is no Post, show a suitable error


@login_required(login_url='/accounts/login')
def dep_view(request):
    context={}
    name=UserModel.objects.filter(user=request.user)
    if name:
        for name in name:
            #department
           if name.details.department:
            pinfo=Post.objects.filter(userdetails__department=name.details.department,deleted=False)[::-1]
    else:
            pinfo=Post.objects.filter(userdetails__department=None)
    context['filter'] = pinfo         
    return render(request,"firsthandapp/filter.html" ,context)


@login_required(login_url='/accounts/login')
def year_view(request):
    context={}
    name=UserModel.objects.filter(user=request.user)
    if name:
        for name in name:
            #department
               pinfo=Post.objects.filter(userdetails__year=name.details.year,deleted=False)[::-1]
    else:
            pinfo=Post.objects.filter(userdetails__year=None)
    context['filter'] = pinfo
    return render(request,"firsthandapp/filter.html" ,context)