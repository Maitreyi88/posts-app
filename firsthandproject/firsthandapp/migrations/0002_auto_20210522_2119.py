# Generated by Django 3.1.1 on 2021-05-22 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firsthandapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='deleted',
            field=models.BooleanField(null=True),
        ),
        migrations.AddField(
            model_name='post',
            name='time',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='usermodeldetails',
            name='city',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='usermodeldetails',
            name='department',
            field=models.CharField(blank=True, choices=[('EN', 'Electronics and Telecommunication'), ('CS', 'Computer Science'), ('PR', 'Printing'), ('IT', 'Information Technology'), ('ME', 'Mechanical'), ('EE', 'Electrical')], max_length=2),
        ),
        migrations.AddField(
            model_name='usermodeldetails',
            name='favourite_color',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AddField(
            model_name='usermodeldetails',
            name='year',
            field=models.CharField(blank=True, choices=[('TE', 'Third Year'), ('BE', 'Final Year'), ('SE', 'Second Year'), ('FE', 'First Year')], max_length=2),
        ),
    ]
